package xRootdprotocolMockServer

import(
  "net"
  "strconv"
)


func CreateServer(port chan string,method string) {

	listen, err := net.Listen("tcp", CONNECTION_HOST + ":" + CONNECTION_PORT)

  checkError(err)

  defer listen.Close()

  port <- strconv.Itoa(listen.Addr().(*net.TCPAddr).Port)

  for{

      server, err := listen.Accept()

      checkError(err)

        switch  method {
        case "SendHandshake":

            go PrepareSendHandshakeresponse(server)

          case "SendProtocol":
              go PrepareSendProtocolresponse(server)

          case "SendPing":
              go PrepareSendPingresponse(server)

          case "SendLogin":
            go PrepareSendLoginresponse(server)

          case "SendInvalid":
          go PrepareSendInvalidresponse(server)
      }

      }
    }



func Createclient(PORT string)(client net.Conn,err error){

  client, err = net.Dial("tcp", CONNECTION_HOST + ":" + PORT)

  return client,err
}

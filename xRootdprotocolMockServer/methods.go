package xRootdprotocolMockServer

import(
  "net"
  "encoding/binary"
  "io"
  "reflect"
)

func PrepareSendPingresponse(server net.Conn){

  clientrequest := make([]byte,24)

  defer server.Close()

  io.ReadFull(server,clientrequest)

  expected := []byte{190 ,239 ,11 ,195 ,0 ,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0}

    if reflect.DeepEqual(expected, clientrequest) {

      server.Write([]byte{190, 239, 0, 0, 0, 0, 0 ,0})
  }
}


func PrepareSendProtocolresponse(server net.Conn){

  clientrequest := make([]byte,24)

  defer server.Close()

  io.ReadFull(server,clientrequest)

  expected := []byte{190 ,239 ,11 ,190 ,0 ,0 ,3 ,16 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0, 0, 0, 0 ,0 ,0}


  if reflect.DeepEqual(expected, clientrequest) {

      server.Write([]byte{190, 239, 0, 0, 0, 0, 0, 8, 0, 0, 3, 16, 0, 0, 0, 1})
  }
}


func PrepareSendLoginresponse(server net.Conn) {

  clientrequest := make([]byte,24)

  defer server.Close()

  io.ReadFull(server,clientrequest)

  expected := []byte{190,239, 11, 191, 0, 0, 0, 0, 103, 111, 112, 104, 101, 114, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  if reflect.DeepEqual(expected, clientrequest) {
    server.Write([]byte{190, 239, 11, 191, 0, 0, 0, 0})
  }
}


func PrepareSendInvalidresponse(server net.Conn){

  clientrequest := make([]byte,24)

  defer server.Close()

  io.ReadFull(server,clientrequest)

  expected := []byte{190, 239, 0 ,0, 0, 0 ,0 ,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  if reflect.DeepEqual(expected, clientrequest) {

      server.Write([]byte{190 ,239 ,15 ,163 ,0 ,0 ,0 ,34 ,0 ,0 ,11 ,185 ,82 ,101 ,113, 117, 105, 114 ,101 ,100 ,32, 97 ,114 ,103 ,117, 109 ,101 ,110 ,116, 32, 110, 111 ,116 ,32 ,112, 114 ,101, 115 ,101, 110, 116, 0})
  }
}




func PrepareSendHandshakeresponse(server net.Conn){

  clientrequest := make([]byte,20)

  defer server.Close()

  io.ReadFull(server,clientrequest)

  if binary.BigEndian.Uint32(clientrequest[0:]) == 0 && binary.BigEndian.Uint32(clientrequest[4:]) == 0 && binary.BigEndian.Uint32(clientrequest[8:]) == 0 && binary.BigEndian.Uint32(clientrequest[12:]) == 4 && binary.BigEndian.Uint32(clientrequest[16:]) == 2012 {

  response := make([]byte, 16)

  binary.BigEndian.PutUint32(response[4:], 8)
  binary.BigEndian.PutUint32(response[8:], 784)
  binary.BigEndian.PutUint32(response[12:], 1)
  server.Write(response)
  }
}

package main

import(
  "testing"
  "xrootd-test-client/XRootdprotocol"
  "xrootd-test-client/xRootdprotocolMockServer"
  "reflect"
)





func TestSendHandshake(t *testing.T){

  port := make(chan string)

  method := "SendHandshake"

  go xRootdprotocolMockServer.CreateServer(port,method)

  PORT := <- port

  client, err  := xRootdprotocolMockServer.Createclient(PORT)

  if err != nil {
      t.Error(err.Error())
  }

  defer client.Close()

  bytesToSend :=[]byte{
  			0,0,0,0,
  			0,0,0,0,
  			0,0,0,0,
  			0,0,0,4,
  			0,0,7,220,
  }

  serverType, _ := XRootdprotocol.SendHandshake(client, bytesToSend)

  if serverType != 1 {
		t.Errorf("Test Fail")
  }
}




func TestSendLogin(t *testing.T){

  flag := make(chan string)

  method := "SendLogin"

  go xRootdprotocolMockServer.CreateServer(flag,method)

  PORT := <- flag

  client, err  := xRootdprotocolMockServer.Createclient(PORT)

  defer client.Close()

  if err != nil {
      t.Error(err.Error())
  }

  streamID := [2]byte{0xbe, 0xef}

  response ,_ := XRootdprotocol.SendLogin(client, streamID)

  expectedresponse := []byte{190, 239, 11, 191, 0, 0, 0, 0}


  if reflect.DeepEqual(response, expectedresponse) != true {
		t.Errorf("Test Fail")
  }



}


func TestSendProtocol(t *testing.T){

  port := make(chan string)

  method := "SendProtocol"

  go xRootdprotocolMockServer.CreateServer(port,method)

  PORT := <- port

  client, err  := xRootdprotocolMockServer.Createclient(PORT)

  defer client.Close()

  if err != nil {
      t.Error(err.Error())
  }

  streamID := [2]byte{0xbe, 0xef}

  response,_ := XRootdprotocol.SendProtocol(client, streamID)

  expectedresponse := []byte{190, 239, 0, 0, 0, 0, 0, 8, 0, 0, 3, 16, 0, 0, 0, 1}

  if reflect.DeepEqual(response, expectedresponse) != true {
		t.Errorf("Test Fail")
  }

}



func TestSendPing(t *testing.T){

  port := make(chan string)

  method := "SendPing"

  go xRootdprotocolMockServer.CreateServer(port,method)

  PORT := <- port

  client, err  := xRootdprotocolMockServer.Createclient(PORT)

  defer client.Close()

  if err != nil {
      t.Error(err.Error())
  }

  streamID := [2]byte{0xbe, 0xef}

  response,_ := XRootdprotocol.SendPing(client, streamID)

  expectedresponse := []byte{190, 239, 0, 0, 0, 0, 0 ,0}

  if reflect.DeepEqual(response, expectedresponse) != true {
    t.Errorf("Test Fail")
  }

}


func TestSendInvalid(t *testing.T){


  method := "SendInvalid"

  port := make(chan string)

  go xRootdprotocolMockServer.CreateServer(port,method)

  PORT := <- port

  client, err  := xRootdprotocolMockServer.Createclient(PORT)

  defer client.Close()

  if err != nil {
      t.Error(err.Error())
  }

  streamID := [2]byte{0xbe, 0xef}

  response,_ := XRootdprotocol.SendInvalid(client, streamID)

  expectedresponse := []byte{190 ,239 ,15 ,163 ,0 ,0 ,0 ,34 ,0 ,0 ,11 ,185 ,82 ,101 ,113, 117, 105, 114 ,101 ,100 ,32, 97 ,114 ,103 ,117, 109 ,101 ,110 ,116, 32, 110, 111 ,116 ,32 ,112, 114 ,101, 115 ,101, 110, 116, 0}

  if reflect.DeepEqual(response, expectedresponse) != true {
    t.Errorf("Test Fail")
  }

}

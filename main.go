package main

import (
    "fmt"
    "flag"
    "net"
    "xrootd-test-client/XRootdprotocol"
    "log"
)

func main() {

    var addr string

    flag.StringVar(&addr, "addr", "0.0.0.0:9001", "listen on address")

    flag.Parse()

    //The Dial function connects to a server

    conn,err := net.Dial("tcp",addr)

    if err != nil {
        log.Fatal(err)
    }

    defer conn.Close()

    fmt.Println("connecting to : ", addr,"\n")

    fmt.Println("SendHandshake request Begins","\n")

    serverType, err := XRootdprotocol.SendHandshake(conn, XRootdprotocol.PrepareInitialHandshake())

    fmt.Println("serverType")

    fmt.Println(serverType)

    fmt.Println("SendHandshake request completed","\n")

    streamID := [2]byte{0xbe, 0xef}

    fmt.Println("SendProtocol request Begins","\n")

    XRootdprotocol.SendProtocol(conn, streamID)

    fmt.Println("SendProtocol request completed","\n")

    fmt.Println("SendLogin request Begins","\n")

    XRootdprotocol.SendLogin(conn, streamID)

  fmt.Println("SendLogin request completed","\n")

  fmt.Println("SendPing request Begins","\n")

  XRootdprotocol.SendPing(conn, streamID)

  fmt.Println("SendPing request completed","\n")

  fmt.Println("SendInvalid request Begins","\n")

  XRootdprotocol.SendInvalid(conn, streamID)

  fmt.Println("SendInvalid request completed","\n")

	fmt.Println("completed","\n")
}

package XRootdprotocol

import(
  "encoding/binary"
  "net"
  "io"
  "fmt"
)

/*
Prepare PrepareInitialHandshake prepare Initial Handshake

When a client first connects to the XRootd server,
it must perform a special handshake.
This handshake will determine whether the client is communicating using XRootd
protocol or another protocol hosted by the server.
The handshake consists of the client sending 20 bytes, as follows:
kXR_int32 0
kXR_int32 0
kXR_int32 0
kXR_int32 4 (network byte order)
kXR_int32 2012(network byte order
(see http://xrootd.org/doc/dev45/XRdv310.pdf)


XRootd protocol , servers should respond, as follows:

streamid: kXR_char smid[2]
status: kXR_unt16 0
msglen: kXR_int32 rlen
msgval1 : kXR_int32 pval
msgval2: kXR_int32 flag

Where:
smid  - is the initial streamid.The smid for the initial response is always two null characters (i.e., ‘\0’)

rlen  - is the binary response length (e.g., 8 for the indicated response).

pval  - is  the binary protocol version number.

flag  - is additional bit-encoded information about the server; as follows:
        kXR_DataServer-This is a data server.
        KXR_LBalServer-This is a load-balancing server
*/

/*
// SendHandshake InitialHandshake.
func SendHandshake(conn net.Conn) error {

	prepareInitialHandshakevaluebyte := PrepareInitialHandshake()

	_, err := conn.Write(prepareInitialHandshakevaluebyte)

  if err != nil{
    log.Fatal(err)
		return err
	}

  //XRootdprotocol servers response
	xRootdserverresponse := make([]byte, 16)

  binary.Read(conn, binary.BigEndian, &xRootdserverresponse)

  fmt.Println("xRootdserverresponse : ",xRootdserverresponse, "\n")

  status := binary.BigEndian.Uint16(xRootdserverresponse[2:])

  fmt.Println("status : ",status, "\n")




  typeofserver := binary.BigEndian.Uint32(xRootdserverresponse[12:])

	if typeofserver == 1 {
		fmt.Println("typeofserver : kXR_DataServer", "\n")
	} else if typeofserver == 0 {
		fmt.Println("typeofserver : KXR_LBalServer", "\n")
	}

  return err
}
*/

func SendHandshake(conn net.Conn, bytesToSend []byte) (int, error) {

  _, err := conn.Write(bytesToSend)

  if err != nil{
		return -1,err
	}

	xRootdserverresponse := make([]byte, 16)


	if _, readErr := io.ReadFull(conn,xRootdserverresponse); readErr != nil {
		return -1,readErr
	}

  rlen := binary.BigEndian.Uint32(xRootdserverresponse[4:])

  fmt.Println("rlen : ",rlen, "\n")

  pval := binary.BigEndian.Uint32(xRootdserverresponse[8:])

  fmt.Println("pval : ",pval, "\n")

	serverType := binary.BigEndian.Uint32(xRootdserverresponse[12:])

/*
	if serverType == 1 {
		fmt.Println("DataServer")
	} else if serverType == 0 {
		fmt.Println("LoadBalancer")
	}

*/
  return int(serverType),err


}


/*
Implementation of kXR_login

Requests
  request sent to the server are a mixture of ASCII and binary. All requests, other
  than the initial handshake request, have the same format, as follows:

  kXR_char streamid[2]
  kXR_unt16 requestid
  kXR_char requestid
  kXR_int32 dlen
  kXR_char data[dlen]


Response
kXR_char streamid[2]
kXR_unt16 status
kXR_int32 dlen
kXR_char data[dlen]

*/

//SendProtocol implements send method.
func SendProtocol(conn net.Conn, streamID [2]byte) ([]byte,error) {

  prepareSendProtocolevalue := preparevalue()

  copy(prepareSendProtocolevalue[0:],streamID[0:])

  binary.BigEndian.PutUint16(prepareSendProtocolevalue[2:], kXR_protocol)

  binary.BigEndian.PutUint32(prepareSendProtocolevalue[4:],kXR_pval)

  _, err := conn.Write(prepareSendProtocolevalue)


  if err != nil{
    return nil, err
  }

  //XRootdprotocol servers response

  xRootdserverresponse := make([]byte, 16)

  binary.Read(conn, binary.BigEndian, &xRootdserverresponse)


  fmt.Println(xRootdserverresponse)

  return xRootdserverresponse, err
}


/*
Implementation of kXR_login

Purpose: Initialize a server connection.

Request format
    kXR_char streamid[2]
    kXR_unt16 kXR_login
    kXR_int32 pid
    kXR_char username[8]
    kXR_char reserved
    kXR_char ability
    kXR_char capver[1]
    kXR_char role[1]
    kXR_int32 tlen
    kXR_char token[tlen]



Normal Response (server < 2.4.0 | client < 1.0)
    kXR_char streamid[2]
    kXR_unt16 0
    kXR_int32 slen
    kXR_char sec[slen]

Normal Response (server >= 2.4.0 & client > 0.0)
  kXR_char streamid[2]
  kXR_unt16 0
  kXR_int32 slen+16
 kXR_char sessid[16]
kXR_char sec[slen]
*/

//func SendLogin send login request to server.
func SendLogin(conn net.Conn, streamID [2]byte) ([]byte, error) {

	prepareSendLoginevalue := preparevalue()

	copy(prepareSendLoginevalue[0:], streamID[0:])

	binary.BigEndian.PutUint16(prepareSendLoginevalue[2:], kXR_login)

	copy(prepareSendLoginevalue[8:], []byte(username))

  _, err := conn.Write(prepareSendLoginevalue)

  if err != nil{
		return nil, err
	}

	xRootdserverresponse := make([]byte,8)

	binary.Read(conn, binary.BigEndian, &xRootdserverresponse)


  slen := binary.BigEndian.Uint32(xRootdserverresponse[4:])

	if slen != 0 {
		sec := make([]byte, 8+slen)
		binary.Read(conn, binary.BigEndian, sec[8:])
		copy(sec[0:8],xRootdserverresponse[0:])
		xRootdserverresponse = sec
		return nil,err
	}

  fmt.Println(xRootdserverresponse)


	return xRootdserverresponse, err
}

/*
Implementation of kXR_login

Purpose: Bind a socket to a pre-existing session.

Request
  kXR_char streamid[2]
  kXR_unt16 kXR_bind
  kXR_char sessid[16]
  kXR_int32 0

  Normal Response
    kXR_char streamid[2]
    kXR_unt16 0
    kXR_int32 1
    kXR_char pathid
*/

//func SendPing Implements ping.
func SendPing(conn net.Conn, streamID [2]byte) ([]byte,error) {

  prepareSendPingvalue := preparevalue()

	copy(prepareSendPingvalue[0:], streamID[0:])

	binary.BigEndian.PutUint16(prepareSendPingvalue[2:], kXR_ping)

	binary.BigEndian.PutUint32(prepareSendPingvalue[20:], 0)

	_, err := conn.Write(prepareSendPingvalue)

	if err != nil{
		return nil,err
	}

	xRootdserverresponse := make([]byte,8)

	binary.Read(conn, binary.BigEndian, &xRootdserverresponse)

  fmt.Println(xRootdserverresponse)

	return xRootdserverresponse,err

}


//SendInvalid check for invalid request.
func SendInvalid(conn net.Conn, streamID [2]byte) ([]byte,error) {

  prepareSendInvalidvalue := preparevalue()

	copy(prepareSendInvalidvalue[0:],streamID[0:])

	binary.BigEndian.PutUint16(prepareSendInvalidvalue[2:], 0)


	_, err := conn.Write(prepareSendInvalidvalue)

	if err != nil{
		return nil,err
	}

	xRootdserverresponse := make([]byte,8)

	binary.Read(conn, binary.BigEndian, &xRootdserverresponse)

	rlen := binary.BigEndian.Uint32(xRootdserverresponse[4:])

	if rlen != 0 {

		data := make([]byte, 8 + rlen)

		binary.Read(conn, binary.BigEndian, data[8:])

		copy(data[0:8],xRootdserverresponse[0:])

		xRootdserverresponse = data

		fmt.Println("status code:",binary.BigEndian.Uint16(xRootdserverresponse[2:]))

		fmt.Println("message:",string(xRootdserverresponse[12:]))

    return xRootdserverresponse, err
	}

	return xRootdserverresponse, err
}
